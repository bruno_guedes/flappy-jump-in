//
//  LeaderboardViewController.m
//  Flappy Jump-in
//
//  Created by Bruno Guedes on 5/04/2014.
//  Copyright (c) 2014 Mi9. All rights reserved.
//

#import "LeaderboardViewController.h"
#import "GlobalConfig.h"

@interface LeaderboardViewController ()

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@end

@implementation LeaderboardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[GlobalConfig baseURL]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onDoneBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
