//
//  AppDelegate.h
//  Flappy Jump-in
//
//  Created by Bruno Guedes on 1/03/2014.
//  Copyright (c) 2014 Mi9. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
