//
//  MyScene.h
//  Flappy Jump-in
//

//  Copyright (c) 2014 Mi9. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@protocol FlappyJumpinSceneDelegate <NSObject>

@required
- (void)gameOverForPlayerName:(NSString *)playerName withScore:(NSInteger)score;
- (void)changePlayer;

@end

@interface FlappyJumpinScene : SKScene <SKPhysicsContactDelegate>

@property(nonatomic, copy) NSString *playerName;

@property(nonatomic, assign) id<FlappyJumpinSceneDelegate> delegate;

@end
