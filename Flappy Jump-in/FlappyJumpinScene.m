//
//  MyScene.m
//  Flappy Jump-in
//
//  Created by Bruno Guedes on 1/03/2014.
//  Copyright (c) 2014 Mi9. All rights reserved.
//

#import "FlappyJumpinScene.h"

@interface FlappyJumpinScene ()

@property (nonatomic, retain) SKSpriteNode *flappy;
@property (nonatomic, assign) CFTimeInterval lastUpdateTimeInterval;
@property (nonatomic, assign) BOOL lastSquareIsAtBottom;
@property (nonatomic, assign) NSInteger score;
@property (nonatomic, retain) SKLabelNode *scoreLabel;
@property (nonatomic, retain) SKLabelNode *changePlayerLabel;

@end

@implementation FlappyJumpinScene

static const uint32_t adCategory = 0x1 << 0;
static const uint32_t videoCategory = 0x1 << 1;
static const uint32_t flappyCategory = 0x1 << 2;
static const NSInteger flappyPosX = 50;

- (id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        self.playerName = @"Player";
        self.backgroundColor = [SKColor colorWithRed:0.15 green:0.15 blue:0.3 alpha:1.0];
        
        self.physicsWorld.gravity = CGVectorMake(0.0, -5.8);
        self.physicsWorld.contactDelegate = self;
        
        [self setupScene];
        
        [self lostMode];
    }
    return self;
}

- (void)setPlayerName:(NSString *)playerName {
    _playerName = playerName;
    [self setupScene];
    
    [self lostMode];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {

    UITouch *touch = [touches anyObject];
    
    if (self.flappy) {
        CGFloat impulse = 15000;
        if (self.flappy.position.y < CGRectGetHeight(self.scene.frame) - 100) {
            [self.flappy.physicsBody applyImpulse:CGVectorMake(0, impulse)];
        }
    } else {
        if ([self.changePlayerLabel containsPoint:[touch locationInNode:self]]) {
            [self.delegate changePlayer];
        } else {
            [self addFlappy];
        }
    }
}

- (void)setupScene {
    
    self.score = 0;
    [self.scene removeAllChildren];
    
    self.scoreLabel = [SKLabelNode labelNodeWithFontNamed:@"Avenir-Medium"];
    self.scoreLabel.fontSize = 30;
    self.scoreLabel.position = CGPointMake(self.scene.size.width/2, self.scene.size.height - 100);
    self.scoreLabel.zPosition = 1000;
    [self addChild:self.scoreLabel];
    
    self.scoreLabel.text = [NSString stringWithFormat:@"%@: 0", self.playerName];
}

- (void)addFlappy {
    
    [self setupScene];
    
    self.flappy = [SKSpriteNode spriteNodeWithImageNamed:@"jump-in_logo"];
    self.flappy.size = CGSizeMake(50, 50);
    self.flappy.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.flappy.size];
    self.flappy.physicsBody.mass = 50;
    self.flappy.physicsBody.categoryBitMask = flappyCategory;
    self.flappy.physicsBody.contactTestBitMask = adCategory;
    self.flappy.physicsBody.usesPreciseCollisionDetection = YES;
    
    self.flappy.position = CGPointMake(flappyPosX, 300);
    
    [self addChild:self.flappy];
}

- (void)addSquare {
    NSInteger height = arc4random_uniform(550) + 100;
    NSInteger yPos = self.lastSquareIsAtBottom ? 0 : CGRectGetHeight(self.scene.frame);
    self.lastSquareIsAtBottom = !self.lastSquareIsAtBottom;
    
    SKSpriteNode *adNode = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(30, height)];
    NSInteger initialPosX = 320;
    adNode.position = CGPointMake(initialPosX, yPos);
    
    adNode.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:adNode.size];
    adNode.physicsBody.dynamic = NO;
    adNode.physicsBody.categoryBitMask = adCategory;
    adNode.physicsBody.usesPreciseCollisionDetection = YES;
    
    float velocity = 100;
    SKAction *actionMove = [SKAction moveTo:CGPointMake(flappyPosX, yPos) duration:(320-flappyPosX)/velocity];
    SKAction *increaseScore =[SKAction runBlock:^{
        self.score++;
        self.scoreLabel.text = [NSString stringWithFormat:@"%@: %d", self.playerName, self.score];
    }];
    SKAction *actionMoveOutOfScreen = [SKAction moveTo:CGPointMake(-flappyPosX, yPos) duration:(2*flappyPosX)/velocity];
    SKAction *actionMoveDone = [SKAction removeFromParent];
    [adNode runAction:[SKAction sequence:@[actionMove, increaseScore, actionMoveOutOfScreen, actionMoveDone]]];
    
    [self addChild:adNode];
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    if (self.flappy.position.x < 0 || self.flappy.position.y < 0) {
        [self lostMode];
    } else if (self.flappy) {
        CFTimeInterval timeSinceLast = currentTime - self.lastUpdateTimeInterval;
        if (timeSinceLast > 1.5) {
            [self addSquare];
            self.lastUpdateTimeInterval = currentTime;
        }
    }
}

- (void)lostMode {
    
    if (!self.playerName) {
        [self.delegate changePlayer];
    }
    
    [self.flappy removeFromParent];
    self.flappy = nil;

    SKLabelNode *myLabel = [SKLabelNode labelNodeWithFontNamed:@"Avenir-Medium"];

    myLabel.text = @"Tap to start again.";
    myLabel.fontSize = 30;
    myLabel.position = CGPointMake(CGRectGetMidX(self.frame),
                                   CGRectGetMidY(self.frame));

    [self addChild:myLabel];
    
    self.changePlayerLabel = [SKLabelNode labelNodeWithFontNamed:@"Avenir-Medium"];
    
    self.changePlayerLabel.text = @"New Player";
    self.changePlayerLabel.fontSize = 30;
    self.changePlayerLabel.position = CGPointMake(CGRectGetMidX(self.frame),
                                   CGRectGetMidY(self.frame)-200);
    
    [self addChild:self.changePlayerLabel];
    
    [self.delegate gameOverForPlayerName:self.playerName withScore:self.score];
}

#pragma mark - SKPhysicsContactDelegate

- (void)didBeginContact:(SKPhysicsContact *)contact {
    SKPhysicsBody *firstBody, *secondBody;
    
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
    {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    }
    else
    {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    
    if ((firstBody.categoryBitMask & secondBody.categoryBitMask) == 0) {
        [self lostMode];
    }
}

@end
