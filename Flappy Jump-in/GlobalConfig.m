//
//  GlobalConfig.m
//  Flappy Jump-in
//
//  Created by Bruno Guedes on 6/04/2014.
//  Copyright (c) 2014 Mi9. All rights reserved.
//

#import "GlobalConfig.h"

@implementation GlobalConfig

+ (instancetype) sharedInstance {
	DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        return [[self alloc] init];
    });
}

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

+ (NSURL *)baseURL {
    NSString *url = [[NSUserDefaults standardUserDefaults] valueForKey:@"base_url"];
    return [NSURL URLWithString:url];
}


@end
