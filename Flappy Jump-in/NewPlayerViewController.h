//
//  NewPlayerViewController.h
//  Flappy Jump-in
//
//  Created by Bruno Guedes on 6/04/2014.
//  Copyright (c) 2014 Mi9. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NewPlayerDelegate <NSObject>

@required
- (void)playerName:(NSString *)playerName;

@end

@interface NewPlayerViewController : UIViewController

@property (nonatomic, assign) id<NewPlayerDelegate> delegate;

@end
