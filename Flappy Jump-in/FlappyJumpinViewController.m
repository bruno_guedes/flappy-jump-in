//
//  ViewController.m
//  Flappy Jump-in
//
//  Created by Bruno Guedes on 1/03/2014.
//  Copyright (c) 2014 Mi9. All rights reserved.
//

#import "FlappyJumpinViewController.h"
#import "FlappyJumpinScene.h"
#import "LeaderboardViewController.h"
#import "GlobalConfig.h"
#import "NewPlayerViewController.h"

@interface FlappyJumpinViewController () <NewPlayerDelegate, FlappyJumpinSceneDelegate>

@property(nonatomic, copy) NSString *playerName;
@property(nonatomic, retain) FlappyJumpinScene *scene;

@end

@implementation FlappyJumpinViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Configure the view.
    SKView * skView = (SKView *)self.view;
    
    // Create and configure the scene.
    self.scene = [FlappyJumpinScene sceneWithSize:skView.bounds.size];
    self.scene.scaleMode = SKSceneScaleModeAspectFill;
    self.scene.delegate = self;
    
    // Present the scene.
    [skView presentScene:self.scene];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *destinationViewController = [[segue destinationViewController] topViewController];
    if ([destinationViewController isKindOfClass:[NewPlayerViewController class]]) {
        NewPlayerViewController *newPlayerViewController = (NewPlayerViewController*)destinationViewController;
        newPlayerViewController.delegate = self;
    }
}

#pragma mark - NewPlayerDelegate

- (void)playerName:(NSString *)playerName {
    self.playerName = playerName;
    self.scene.playerName = self.playerName;
}

#pragma mark - FlappyJumpinSceneDelegate

- (void)changePlayer {
    [self performSegueWithIdentifier:@"NewPlayerSegue" sender:self];
}

- (void)gameOverForPlayerName:(NSString *)playerName withScore:(NSInteger)score {
    
    NSURL *updateScoreURL = [NSURL URLWithString:[NSString stringWithFormat:@"update/?name=%@&score=%ld", playerName, (long)score] relativeToURL:[GlobalConfig baseURL]];

    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:updateScoreURL]
                                       queue:[NSOperationQueue currentQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {

                           }];
    
    
    [self performSegueWithIdentifier:@"LeaderboardSegue" sender:self];
}

@end
