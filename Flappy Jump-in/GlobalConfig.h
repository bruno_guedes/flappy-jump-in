//
//  GlobalConfig.h
//  Flappy Jump-in
//
//  Created by Bruno Guedes on 6/04/2014.
//  Copyright (c) 2014 Mi9. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef DEFINE_SHARED_INSTANCE_USING_BLOCK
#define DEFINE_SHARED_INSTANCE_USING_BLOCK(block) \
static dispatch_once_t pred = 0; \
__strong static id _sharedObject = nil; \
dispatch_once(&pred, ^{ \
_sharedObject = block(); \
}); \
return _sharedObject;
#endif


@interface GlobalConfig : NSObject

+ (instancetype) sharedInstance;

+ (NSURL *)baseURL;

@end
