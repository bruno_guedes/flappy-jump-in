//
//  NewPlayerViewController.m
//  Flappy Jump-in
//
//  Created by Bruno Guedes on 6/04/2014.
//  Copyright (c) 2014 Mi9. All rights reserved.
//

#import "NewPlayerViewController.h"

@interface NewPlayerViewController ()

@property (strong, nonatomic) IBOutlet UITextField *playerNameTextField;

@end

@implementation NewPlayerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onSaveButton:(id)sender {
    if (self.playerNameTextField.text.length > 0) {
        [self.delegate playerName:self.playerNameTextField.text];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"please type a player name" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil] show];
    }
}

- (IBAction)onCancelButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
