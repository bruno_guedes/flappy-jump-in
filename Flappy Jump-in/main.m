//
//  main.m
//  Flappy Jump-in
//
//  Created by Bruno Guedes on 1/03/2014.
//  Copyright (c) 2014 Mi9. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
